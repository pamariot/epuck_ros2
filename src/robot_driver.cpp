#include <memory>
#include <iostream>

#include <rclcpp/rclcpp.hpp>
#include <std_msgs/msg/float64_multi_array.hpp>
#include <sensor_msgs/msg/joint_state.hpp>
#include <sensor_msgs/msg/image.hpp>
#include <sensor_msgs/msg/range.hpp>
#include <cv_bridge/cv_bridge.h>

#include <fmt/format.h>

#include <epuck/robot_driver.hpp>

using sensor_msgs::msg::Image;
using sensor_msgs::msg::JointState;
using sensor_msgs::msg::Range;
using std_msgs::msg::Float64MultiArray;

class EPuckController : public rclcpp::Node {
public:
    EPuckController() : Node("epuck_controller"), robot_{RobotType::V2} {
        declare_parameter("ip");
        declare_parameter("robot_version", 2);

        cmd_subscription_ = create_subscription<Float64MultiArray>(
            "/wheels_velocity_controller/commands", 10,
            [this](Float64MultiArray::SharedPtr msg) {
                joint_cmd_callback(msg);
            });

        joint_state_publisher_ =
            create_publisher<JointState>("/joint_states", 10);

        camera_publisher_ = create_publisher<Image>("/camera/image_raw", 10);

        for (size_t i = 0; i < 8; i++) {
            laser_publishers_.emplace_back(
                create_publisher<Range>(fmt::format("/ps{}/out", i), 10));
        }

        joint_state_.name = {"left_joint", "right_joint"};
        joint_state_.position.resize(2, 0);
        joint_state_.velocity.resize(2, 0);
        joint_state_.effort.resize(2, 0);

        robot_.controller = Controller::SetWheelCmd;

        int robot_version = get_parameter("robot_version").as_int();

        const auto ip_address = get_parameter("ip").as_string();

        switch (robot_version) {
        case 0:
            robot_.type = RobotType::VREP;
            driver_ = make_driver(robot_, ip_address);
            break;
        case 1:
            robot_.type = RobotType::V1;
            driver_ = make_driver(robot_, ip_address);
            break;
        case 2:
            robot_.type = RobotType::V2;
            driver_ = make_driver(robot_, ip_address);
            break;
        default:
            throw std::range_error("Invalid robot version");
        }

        if (not driver_->init()) {
            RCLCPP_ERROR(
                get_logger(),
                "Failed to initialize the communication with the robot");
            std::exit(1);
        }

        std_msgs::msg::Header header;
        header.frame_id = "camera_link";
        image_ = cv_bridge::CvImage(header, "rgb8", robot_.camera_image);
    }

    void process() {
        driver_->read();
        driver_->sendCmd();

        joint_state_.position[0] = robot_.wheels_state.left_position;
        joint_state_.position[1] = robot_.wheels_state.right_position;

        joint_state_.velocity[0] = robot_.wheels_state.left_velocity;
        joint_state_.velocity[1] = robot_.wheels_state.right_velocity;

        joint_state_publisher_->publish(joint_state_);

        camera_publisher_->publish(*image_.toImageMsg());

        for (size_t i = 0; i < 8; i++) {
            auto range = Range{};
            range.range = robot_.proximity_sensors.ir_calibrated[i];
            laser_publishers_[i]->publish(range);
        }
    }

private:
    void joint_cmd_callback(const Float64MultiArray::SharedPtr& msg) {
        if (msg->data.size() != 2) {
            RCLCPP_INFO(
                get_logger(),
                fmt::format(
                    "Invalid number of joint commands received: {} != 2",
                    msg->data.size()));
            return;
        }

        if (robot_.type == RobotType::VREP) {
            robot_.wheels_command.left_velocity = msg->data[0];
            robot_.wheels_command.right_velocity = msg->data[1];
        } else {
            robot_.wheels_command.left_velocity =
                to_steps_per_second(msg->data[0]);
            robot_.wheels_command.right_velocity =
                to_steps_per_second(msg->data[1]);
        }
    }

    static double to_steps_per_second(double rad_per_sec) {
        const double steps_per_turn = 1000;
        return rad_per_sec * steps_per_turn / (2. * M_PI);
    }

    rclcpp::Subscription<Float64MultiArray>::SharedPtr cmd_subscription_;
    rclcpp::Publisher<JointState>::SharedPtr joint_state_publisher_;
    rclcpp::Publisher<Image>::SharedPtr camera_publisher_;
    std::vector<rclcpp::Publisher<Range>::SharedPtr> laser_publishers_;

    Robot robot_;
    std::unique_ptr<RobotDriver> driver_;
    JointState joint_state_;
    cv_bridge::CvImage image_;
};

int main(int argc, char* argv[]) {
    rclcpp::init(argc, argv);

    auto epuck_controller = std::make_shared<EPuckController>();

    while (rclcpp::ok()) {
        epuck_controller->process();
        rclcpp::spin_some(epuck_controller);
    }

    rclcpp::shutdown();

    return 0;
}