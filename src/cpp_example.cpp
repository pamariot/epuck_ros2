#include <array>

#include <rclcpp/rclcpp.hpp>
#include <std_msgs/msg/float64_multi_array.hpp>
#include <sensor_msgs/msg/joint_state.hpp>
#include <sensor_msgs/msg/range.hpp>
#include <sensor_msgs/msg/image.hpp>
#include <cv_bridge/cv_bridge.h>

#include <opencv2/highgui.hpp>
#include <fmt/format.h>

using sensor_msgs::msg::Image;
using sensor_msgs::msg::JointState;
using sensor_msgs::msg::Range;
using std_msgs::msg::Float64MultiArray;

class EPuckController : public rclcpp::Node {
public:
    EPuckController() : Node("epuck_controller") {
        cmd_publisher_ = create_publisher<Float64MultiArray>(
            "/wheels_velocity_controller/commands", 10);

        joint_state_subscription_ = create_subscription<JointState>(
            "/joint_states", 10,
            [this](JointState::SharedPtr msg) { joint_state_callback(msg); });

        for (size_t i = 0; i < 8; i++) {
            laser_subscriptions_[i] = create_subscription<Range>(
                fmt::format("/ps{}/out", i), 10,
                [this, i](Range::SharedPtr msg) { laser_callback(msg, i); });
        }

        camera_subscription_ = create_subscription<Image>(
            "/camera/image_raw", 10,
            [this](Image::SharedPtr msg) { camera_callback(msg); });

        cmd_.data.resize(2, 0);

        laser_distances_.fill(1000);

        start_time_ = get_clock()->now().seconds();

        using namespace std::chrono_literals;
        run_timer_ = create_wall_timer(100ms, [this] { run(); });
    }

    void run() {
        if (laser_distances_[0] > 0.1) {
            cmd_.data = {1.0, -1.0};
        } else {
            cmd_.data = {0.0, 0.0};
        }

        cmd_publisher_->publish(cmd_);
    }

private:
    void joint_state_callback(const JointState::SharedPtr& msg) const {
        RCLCPP_INFO(
            get_logger(),
            fmt::format(
                "\nWheels\n\tname: {}\n\tposition: {}\n\tvelocity: {}\n",
                fmt::join(msg->name, ", "), fmt::join(msg->position, ", "),
                fmt::join(msg->velocity, ", ")));
    }

    void camera_callback(const Image::SharedPtr& msg) {
        cv_image_ = cv_bridge::toCvCopy(msg);
        cv::imshow("robot camera", cv_image_->image);
        cv::waitKey(1);
    }

    void laser_callback(const Range::SharedPtr& msg, size_t index) {
        laser_distances_[index] = msg->range;
    }

    rclcpp::Publisher<Float64MultiArray>::SharedPtr cmd_publisher_;
    rclcpp::Subscription<JointState>::SharedPtr joint_state_subscription_;
    rclcpp::Subscription<Image>::SharedPtr camera_subscription_;
    std::array<rclcpp::Subscription<Range>::SharedPtr, 8> laser_subscriptions_;
    std::array<double, 8> laser_distances_;
    rclcpp::TimerBase::SharedPtr run_timer_;

    Float64MultiArray cmd_;
    cv_bridge::CvImagePtr cv_image_;
    double start_time_{};
};

int main(int argc, char* argv[]) {
    rclcpp::init(argc, argv);

    auto epuck_controller = std::make_shared<EPuckController>();

    rclcpp::spin(epuck_controller);
    rclcpp::shutdown();

    return 0;
}