<?xml version = "1.0" ?>
<robot name="epuck"
  xmlns:xacro="http://www.ros.org/wiki/xacro">

  <xacro:macro name="epuck_laser" params="name angle">
    <xacro:property name="radius" value="0.036" />

    <link name="${name}_laser_link" />

    <joint name= "base_link_to_${name}_laser" type="fixed">
      <origin xyz="${radius * cos(radians(angle))} ${radius * sin(radians(angle))} 0.034" rpy="0 0 ${radians(angle)}"/>
      <parent link="base_link"/>
      <child link="${name}_laser_link"/>
    </joint>

    <gazebo reference="${name}_laser_link">
      <sensor type="ray" name="${name}_laser">
        <visualize>true</visualize>
        <update_rate>10</update_rate>
        <ray>
          <scan>
            <horizontal>
              <samples>1</samples>
              <min_angle>-0.0001</min_angle>
              <max_angle>0.0001</max_angle>
            </horizontal>
          </scan>
          <range>
            <min>0.001</min>
            <max>0.15</max>
            <resolution>0.001</resolution>
          </range>
        </ray>
        <update_rate>10</update_rate>
        <plugin name="${name}" filename="libgazebo_ros_ray_sensor.so">
          <output_type>sensor_msgs/Range</output_type>
        </plugin>
      </sensor>
    </gazebo>
  </xacro:macro>

  <link name="base_footprint"/>

  <joint name="base_joint" type="fixed">
    <parent link="base_footprint"/>
    <child link="base_link"/>
    <origin xyz="0 0 0" rpy="0 0 0"/>
  </joint>

  <link name="base_link">
    <inertial>
      <origin xyz="9.0274E-05 -0.00041659 0.019845" rpy="0 0 0" />
      <mass value="0.00063515" />
      <inertia ixx="3.2227E-05" ixy="-2.691E-07" ixz="4.7122E-08" iyy="2.4227E-05" iyz="-1.2861E-06" izz="3.3868E-05" />
    </inertial>
    <visual>
      <origin xyz="0 0 0" rpy="0 0 0" />
      <geometry>
        <mesh filename="package://epuck_ros2/meshes/main_body.STL" />
      </geometry>
      <material name="">
        <color rgba="0.75294 0.75294 0.75294 1" />
      </material>
    </visual>
    <collision>
      <origin xyz="0 0 0" rpy="0 0 0" />
      <geometry>
        <mesh filename="package://epuck_ros2/meshes/main_body.STL" />
      </geometry>
    </collision>
  </link>

  <joint name="left_joint" type="continuous">
    <origin xyz="0.0278 0 0.02" rpy="0 0 0" />
    <parent link="base_link" />
    <child link="left_wheel" />
    <axis xyz="-1 0 0" />
  </joint>

  <link name="left_wheel">
    <inertial>
      <origin xyz="-0.00048709 -3.5245E-18 2.4286E-17" rpy="0 0 0" />
      <mass value="0.0040981" />
      <inertia ixx="8.267E-07" ixy="-9.8761E-23" ixz="-8.6429E-13" iyy="4.304E-07" iyz="-2.3376E-22" izz="4.304E-07" />
    </inertial>
    <visual>
      <origin xyz="0 0 0" rpy="0 0 0" />
      <geometry>
        <mesh filename="package://epuck_ros2/meshes/left_wheel.STL" />
      </geometry>
      <material name="">
        <color rgba="0.79216 0.81961 0.93333 1" />
      </material>
    </visual>
    <collision>
      <origin xyz="0 0 0" rpy="0 0 0" />
      <geometry>
        <mesh filename="package://epuck_ros2/meshes/left_wheel.STL" />
      </geometry>
    </collision>
  </link>

  <joint name="right_joint" type="continuous">
    <origin xyz="-0.0278 0 0.02" rpy="0 0 0" />
    <parent link="base_link" />
    <child link="right_wheel" />
    <axis xyz="-1 0 0" />
  </joint>

  <link name="right_wheel">
    <inertial>
      <origin xyz="0.00065056 4.0415E-18 7.2858E-17" rpy="0 0 0" />
      <mass value="0.0038059" />
      <inertia ixx="8.2274E-07" ixy="-1.0753E-22" ixz="8.6429E-13" iyy="4.2585E-07" iyz="2.3307E-22" izz="4.2585E-07" />
    </inertial>
    <visual>
      <origin xyz="0 0 0" rpy="0 0 0" />
      <geometry>
        <mesh filename="package://epuck_ros2/meshes/right_wheel.STL" />
      </geometry>
      <material name="">
        <color rgba="0.79216 0.81961 0.93333 1" />
      </material>
    </visual>
    <collision>
      <origin xyz="0 0 0" rpy="0 0 0" />
      <geometry>
        <mesh filename="package://epuck_ros2/meshes/right_wheel.STL" />
      </geometry>
    </collision>
  </link>

  <joint name= "base_link_to_camera" type="fixed">
    <origin xyz="0 0.0307 0.03" rpy="0 0 1.57"/>
    <parent link="base_link"/>
    <child link="camera_link"/>
  </joint>

  <link name="camera_link"/>

  <ros2_control name="GazeboSystem" type="system">
    <hardware>
      <plugin>gazebo_ros2_control/GazeboSystem</plugin>
    </hardware>
    <joint name="left_joint">
      <command_interface name="velocity">
        <param name="min">-3.75</param>
        <param name="max">3.75</param>
      </command_interface>
      <state_interface name="position">
        <param name="initial_value">0</param>
      </state_interface>
      <state_interface name="velocity"/>
    </joint>
    <joint name="right_joint">
      <command_interface name="velocity">
        <param name="min">-3.75</param>
        <param name="max">3.75</param>
      </command_interface>
      <state_interface name="position">
        <param name="initial_value">0</param>
      </state_interface>
      <state_interface name="velocity"/>
    </joint>
  </ros2_control>

  <gazebo>
    <plugin filename="libgazebo_ros2_control.so" name="gazebo_ros2_control">
      <parameters>$(find epuck_ros2)/config/epuck_controllers.yaml</parameters>
    </plugin>
  </gazebo>

  <gazebo reference="base_link">
    <material>Gazebo/Grey</material>
  </gazebo>

  <gazebo reference="camera_link">
    <sensor type="camera" name="camera1">
      <update_rate>10.0</update_rate>
      <always_on>1</always_on>
      <camera name="camera">
        <horizontal_fov>1.3962634</horizontal_fov>
        <image>
          <width>640</width>
          <height>480</height>
          <format>R8G8B8</format>
        </image>
        <clip>
          <near>0.02</near>
          <far>300</far>
        </clip>
        <distortion>
          <k1>0</k1>
          <k2>0</k2>
          <k3>0</k3>
          <p1>0</p1>
          <p2>0</p2>
          <center>0.5 0.5</center>
        </distortion>
      </camera>
      <plugin name="camera_controller" filename="libgazebo_ros_camera.so">
        <camera_name>camera</camera_name>
        <frame_name>camera_link</frame_name>
        <hack_baseline>0.07</hack_baseline>
        <ros>
          <argument>--ros-args --remap image_raw:=image_raw</argument>
          <argument>--ros-args --remap camera_info:=camera_info</argument>
        </ros>
      </plugin>
    </sensor>
  </gazebo>

  <xacro:epuck_laser name="ps0" angle="75" />
  <xacro:epuck_laser name="ps1" angle="45" />
  <xacro:epuck_laser name="ps2" angle="0" />
  <xacro:epuck_laser name="ps3" angle="300" />
  <xacro:epuck_laser name="ps4" angle="240" />
  <xacro:epuck_laser name="ps5" angle="180" />
  <xacro:epuck_laser name="ps6" angle="135" />
  <xacro:epuck_laser name="ps7" angle="105" />

  <gazebo reference="right_wheel">
    <selfCollide>true</selfCollide>
    <mu1>10</mu1>
    <mu2>10</mu2>
    <material>Gazebo/White</material>
  </gazebo>

  <gazebo reference="left_wheel">
    <selfCollide>true</selfCollide>
    <mu1>10</mu1>
    <mu2>10</mu2>
    <material>Gazebo/White</material>
  </gazebo>

  <gazebo reference="base_link">
    <selfCollide>true</selfCollide>
    <mu1>0</mu1>
    <mu2>0</mu2>
  </gazebo>

</robot>
