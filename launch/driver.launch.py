import launch
import launch_ros
from launch import LaunchDescription
from launch.substitutions import LaunchConfiguration


def generate_launch_description():
    driver_node = launch_ros.actions.Node(
        package='epuck_ros2',
        executable='robot_driver',
        output='screen',
        parameters=[
            {'robot_version': LaunchConfiguration('robot_version'),
             'ip': LaunchConfiguration('ip')}]
    )

    return LaunchDescription([
        launch.actions.DeclareLaunchArgument(name='robot_version', default_value="2",
                                             description='Robot version in use (1 or 2)'),
        launch.actions.DeclareLaunchArgument(name='ip',
                                             description='Robot ip address'),
        driver_node
    ])
